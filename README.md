# Bundle PlantUML

Ce bundle permet de générer des diagrammes de classe à partir des sources d'une application PHP.

Il utilise à la fois l'[API de Reflection de PHP](http://php.net/manual/fr/book.reflection.php) et les métadonnées
de [Doctrine ORM](http://www.doctrine-project.org/projects/orm.html).

Le rendu se fait avec [PlantUML](http://plantuml.com/).

Ce bundle est un fork du projet [https://gitlab.irstea.fr/pole-is/bundles/plantuml-bundle](https://gitlab.irstea.fr/pole-is/bundles/plantuml-bundle).

## Installation

Installer le package via composer :

    composer require --dev reech/plantuml-bundle

Lister le bundle dans le kernel de Symfony :

    // bundles en environnement dev et test
    if (in_array($this->getEnvironment(), array('dev', 'test'))) {
        // ...
        $bundles[] = new Reech\PlantUmlBundle\ReechPlantUmlBundle();
        // ...
    }

Les dépendances sont dans les dépôts standards de Debian :

    sudo apt-get install default-jre-headless

PlantUML peut se télécharger depuis leur site web.

## Configuration

### Principe

Le bundle permet de générer plusieurs graphes différents, c-à-d plusieurs vues différentes des modèles. Il faut décrire
ces vues dans la configuration.

Pour chaque graphe, le bundle par d'une collection de classe de départ, qu'il va analyser, regrouper et décorer.
Certains décorateurs permet de "découvrir" nouvelles classes, par exemple par héritage, associations, ...

Pour chaque vue, il faut définir :
* comment trouver les classes de départ,
* quelles classes tracer et comment les regrouper,
* quelles classes décorer.

Cela correspondent respectiviement aux sections `sources`, `layout` et `decoration` de la configuration de chaque graphe.

*Nota bene* : les graphes dont le nom commence par un point (`.`) sont ignorés. Cela permet de définir des modèles
grâce aux systèmes d'ancres YAML.

### Example

Vous trouverez un exemple de configuration dans le fichier [config/packages/reech_plant_uml.example.yml](config/packages/reech_plant_uml.example.yml).

### Sources

Il y a deux façons de trouver les classes à tracer :
* `entities` : utilise les entités trouvées par le manager désigné par `entity_manager`.
* `classes` : charge toutes les classes trouvées dans les répertoires listés par `directories`.

Les sections `include`ou `exclude` permettent de raffiner la sélection.

### Layout

Le paramètre `namespaces` permet choisir quel style de namespace doit être utiliser  .
* `php`: utilise les namespaces PHP.
* `flat`:  n'utilise pas de namespaces.
* `bundles`: regroupe les classes par bundle Symfony.
* `entities`: regroupe les classes par "alias" Doctrine.

Les sections `include`ou `exclude` permettent de filtrer quelles classes seront filtrées et parcourues.

### Décoration

La décoration consiste à afficher des informations sur chacune des classes. Certains décorateur ajoutent des informations
dans la classe, d'autres ajoutent des liens avec d'autres classes. Sans décorateur, chaque classe ne serait qu'une boîte
avec un nom, et rien d'autre.

Les décorateurs suivants sont basés sur la Reflection du PHP :

* inheritance : affiche les liens d'héritage de classe.
* traits : affiche les liens d'utilisation de trait.
* interfaces : affiche les liens d'implémentation d'interface.
* attributes : liste les attributs.
* methods : liste les méthodes.

Les décorateurs suivants sont basés sur les métadonnées Doctrine ; ils sont ignorés si la classe n'est pas une entité :

* entity : affiche le type d'entité.
* associations : affiche les associations d'entités.
* fields : affiche les définitions des champs (=colonnes) de l'entité.

Les sections `include`ou `exclude` permettent de filtrer quelles classes seront décorées.

### Fonctionnement include/exclude

Les 3 sections peuvent contenir des sections `include` et `exclude`, qui permet de filter les classes par namespace ou répertoire.
Les filtres s'appliquent aux répertoires/namespaces et tous leurs sous-niveaux.

Le fonctionnement est le suivant :
* les sections vides ou absentes n'ont absolument aucun effet.
* S'il y a une section `include`, seuls les répertoires/namespaces listés seront acceptés.
* S'il y a une section `exclude`, les répertoires/namespaces listés seront ignorés.
* Si les deux sections sont présentes, une classe est acceptée si elle est validé par `include` *ET* qu'elle n'est pas ignoré par un `exclude`.

## Usage

### Génération du source du diagramme

La génération se fait par un seule commande, qui pour l'instant ne fait qu'envoyer un fichier sur la sortie standard:

    app/console reech:plantuml:generate mongraph >mongraph.puml

### Rendu graphique

Le rendu graphique se fait avec PlantUML, par exemple, pour générer un SVG :

    app/console reech:plantuml:render -o output_dir mongraph
