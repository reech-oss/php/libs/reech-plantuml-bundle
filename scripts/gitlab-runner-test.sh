#!/usr/bin/env bash

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

pushd "${script_dir}/.."

docker run --rm -it -v $(pwd):$(pwd) --workdir $(pwd) -v /var/run/docker.sock:/var/run/docker.sock --privileged gitlab/gitlab-runner exec docker --docker-privileged --env DOCKER_TLS_CERTDIR= "$@"

popd
