#!/usr/bin/env bash

# set options
# -e:           Exit immediately if a command exits with a non-zero status (but not when chained with "&&" !!!)
# -u:           Treat unset variables as an error when substituting
# -v:           Print shell input lines as they are read
# -x:           Print commands and their arguments as they are executed
# -o pipefail:  The return value of a pipeline is the status of the last command to exit with a non-zero status, or zero if no command exited with a non-zero status
set -euo pipefail

# Only execute in Docker
[[ ! -e /.dockerenv ]] && exit 0

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

pushd "${script_dir}/../../"

apt-get update \
&& DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true apt-get install -y --no-install-recommends curl wget git unzip \
&& cp -p /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini \
&& php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
&& php composer-setup.php --install-dir=/usr/local/bin --filename=composer \
&& php -r "unlink('composer-setup.php');" \
&& composer install \
&& php vendor/bin/grumphp run --testsuite=ci_testing

return_code=$?

popd

exit ${return_code}
