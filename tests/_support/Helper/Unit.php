<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Tests\Helper;

use Codeception\Module;

/**
 * Class Unit.
 */
class Unit extends Module
{
    // here you can define custom actions
    // all public methods declared in helper class will be available in $I
}
