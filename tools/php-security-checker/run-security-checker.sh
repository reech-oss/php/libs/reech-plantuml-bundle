#!/usr/bin/env bash

# set options
# -e:           Exit immediately if a command exits with a non-zero status (but not when chained with "&&" !!!)
# -u:           Treat unset variables as an error when substituting
# -v:           Print shell input lines as they are read
# -x:           Print commands and their arguments as they are executed
# -o pipefail:  The return value of a pipeline is the status of the last command to exit with a non-zero status, or zero if no command exited with a non-zero status
set -euo pipefail

script_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" > /dev/null 2>&1 && pwd)"
project_dir="${script_dir}/../.."
var_cache_dir="${project_dir}/var/cache"
local_php_security_checker_bin="${var_cache_dir}/local-php-security-checker"

if [ ! -d "${var_cache_dir}" ]; then
    mkdir -p "${var_cache_dir}"
fi

if [ ! -f "${local_php_security_checker_bin}" ]; then
    # Retrieve the download URL for the latest version of the local-php-security-checker tool
    download_url=$(curl -s https://api.github.com/repos/fabpot/local-php-security-checker/releases/latest | grep browser_download_url | grep linux_amd64 | cut -d'"' -f 4)

    # Download the local-php-security-checker tool
    wget -qO "${local_php_security_checker_bin}" "${download_url}"
    chmod +x "${local_php_security_checker_bin}"
fi

# Execute
"${local_php_security_checker_bin}" --path="${project_dir}/composer.lock"

