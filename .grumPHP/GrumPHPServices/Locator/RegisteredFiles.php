<?php

declare(strict_types=1);

namespace GrumPHPServices\Locator;

use GrumPHP\Collection\FilesCollection;
use GrumPHP\Locator\RegisteredFiles as RegisteredFilesOriginal;
use Symfony\Component\Finder\SplFileInfo;

/**
 * Class RegisteredFiles.
 */
class RegisteredFiles extends RegisteredFilesOriginal
{
    /**
     * @return FilesCollection
     */
    public function locate(): FilesCollection
    {
        $filesCollection = parent::locate();

        $existingFiles = array_filter(
            $filesCollection->toArray(),
            static function (SplFileInfo $splFileInfo) {
                // Check that the file in git really exists in filesystem
                return $splFileInfo->isFile();
            }
        );

        return new FilesCollection($existingFiles);
    }
}
