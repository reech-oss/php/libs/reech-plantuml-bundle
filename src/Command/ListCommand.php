<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ListCommand.
 */
class ListCommand extends Command
{
    private const CMD_NAME = 'reech:plantuml:list';

    /**
     * @var string[]
     */
    private array $graphNames;

    /**
     * ListCommand constructor.
     *
     * @param string[] $graphNames
     */
    public function __construct(array $graphNames)
    {
        parent::__construct(self::CMD_NAME);
        $this->graphNames = $graphNames;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setName(self::CMD_NAME)
            ->setDescription('Liste les graphes PlantUML configurés.')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        foreach ($this->graphNames as $name) {
            $output->writeln($name);
        }

        return 0;
    }
}
