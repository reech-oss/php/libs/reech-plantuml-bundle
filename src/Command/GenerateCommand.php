<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Command;

use Reech\PlantUmlBundle\Writer\OutputWriter;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\Security\Core\Exception\InvalidArgumentException;

/**
 * Description of ImportAffiliationCommand.
 */
class GenerateCommand extends ContainerAwareCommand
{
    protected function configure(): void
    {
        $this
            ->setName('reech:plantuml:generate')
            ->setDescription('Génère un graphe en PlantUML.')
            ->addArgument('graph', InputArgument::REQUIRED, 'Nom du graphe à générer')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @SuppressWarnings(UnusedFormalParameter)
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $name = $input->getArgument('graph');
        $serviceId = "reech_plant_uml.graph.{$name}";

        if (!$this->getContainer() || !$this->getContainer()->has($serviceId)) {
            throw new InvalidArgumentException("Le graphe '{$name}' n'est pas défini.");
        }

        $writer = new OutputWriter($output);
        $graph = $this->getContainer()->get($serviceId);
        if ($graph) {
            $graph->visitAll();
            $graph->writeTo($writer);
        } else {
            throw new ServiceNotFoundException($serviceId);
        }
    }
}
