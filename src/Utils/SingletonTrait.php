<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Utils;

/**
 * Description of Singleton.
 */
trait SingletonTrait
{
    private static ?self $instance;

    public static function instance(): self
    {
        if (!static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }
}
