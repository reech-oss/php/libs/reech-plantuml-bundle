<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Finder;

use IteratorAggregate;
use Reech\PlantUmlBundle\Model\ToConfigInterface;
use Traversable;

/**
 * Interface FinderInterface.
 */
interface FinderInterface extends Traversable, IteratorAggregate, ToConfigInterface
{
}
