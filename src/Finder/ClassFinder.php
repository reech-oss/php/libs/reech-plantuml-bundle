<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Finder;

use ArrayIterator;
use Reech\PlantUmlBundle\Model\Filter\DirectoryFilter;
use ReflectionClass;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

use function assert;
use function get_class;

/**
 * Description of ClassFinder.
 */
class ClassFinder implements FinderInterface
{
    /**
     * @var string[]
     */
    private array $directories;
    /**
     * @var null|ReflectionClass[]
     */
    private ?array $classes = null;

    /**
     * @param string[] $directories
     */
    public function __construct(array $directories)
    {
        $this->directories = $directories;
    }

    public function getIterator(): ArrayIterator
    {
        if (null === $this->classes) {
            $this->initialize();
        }

        return new ArrayIterator($this->classes);
    }

    public function toConfig(array &$conf): void
    {
        $conf['type'] = 'classes';
        $conf['directories'] = $this->directories;
    }

    /**
     * @SuppressWarnings(PHPMD.ErrorControlOperator)
     */
    private function initialize(): void
    {
        $files = Finder::create()
            ->in($this->directories)
            ->files()
            ->name('*.php')
            ->getIterator()
        ;

        foreach ($files as $file) {
            assert($file instanceof SplFileInfo);
            $path = $file->getPathname();

            try {
                /** @noinspection UsingInclusionOnceReturnValueInspection */
                @include_once $path;
            } catch (Exception $ex) {
                printf("%s: %s (%s)\n", $path, get_class($ex), $ex->getMessage());
            }
        }

        $filter = new DirectoryFilter($this->directories);
        $this->classes = [];
        foreach (get_declared_classes() as $className) {
            $class = new ReflectionClass($className);
            if ($filter->accept($class)) {
                $this->classes[$className] = $class;
            }
        }
    }
}
