<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Finder;

use CallbackFilterIterator;
use Reech\PlantUmlBundle\Model\ClassFilterInterface;

/**
 * Description of FilteringFinder.
 */
class FilteringFinder implements FinderInterface
{
    private FinderInterface $inner;
    private ClassFilterInterface $filter;

    public function __construct(FinderInterface $inner, ClassFilterInterface $filter)
    {
        $this->inner = $inner;
        $this->filter = $filter;
    }

    public function getIterator(): CallbackFilterIterator
    {
        return new CallbackFilterIterator($this->inner->getIterator(), [$this->filter, 'accept']);
    }

    public function toConfig(array &$conf): void
    {
        $this->inner->toConfig($conf);
        $this->filter->toConfig($conf);
    }
}
