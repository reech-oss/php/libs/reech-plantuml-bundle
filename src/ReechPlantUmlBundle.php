<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class PlantUmlBundle.
 */
class ReechPlantUmlBundle extends Bundle
{
}
