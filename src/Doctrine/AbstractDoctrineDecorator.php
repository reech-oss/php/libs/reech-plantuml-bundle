<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Doctrine;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadataFactory;
use Reech\PlantUmlBundle\Model\DecoratorInterface;
use ReflectionClass;

/**
 * Description of AbstractDoctrineDecorator.
 */
abstract class AbstractDoctrineDecorator implements DecoratorInterface
{
    protected ClassMetadataFactory $metadataFactory;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->metadataFactory = $manager->getMetadataFactory();
    }

    /**
     * @param callable             $callback
     * @param null|ReflectionClass $class
     *
     * @return mixed
     */
    protected function withMetadata(callable $callback, ReflectionClass $class = null)
    {
        if (!$class) {
            return null;
        }

        $className = $class->getName();
        if (!$this->metadataFactory->hasMetadataFor($className)) {
            return null;
        }

        return $callback($this->metadataFactory->getMetadataFor($className));
    }
}
