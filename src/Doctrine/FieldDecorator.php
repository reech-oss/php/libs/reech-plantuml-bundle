<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Doctrine;

use Doctrine\ORM\Mapping\ClassMetadata;
use Reech\PlantUmlBundle\Model\ClassVisitorInterface;
use Reech\PlantUmlBundle\Model\Decorator\InheritableItemDecoratorTrait;
use Reech\PlantUmlBundle\Model\NodeInterface;
use ReflectionClass;

/**
 * Description of RelationDecorator.
 */
class FieldDecorator extends AbstractDoctrineDecorator
{
    use InheritableItemDecoratorTrait;

    public function toConfig(array &$conf): void
    {
        $conf['decorators'][] = 'fields';
    }

    /**
     * @param ReflectionClass $class
     *
     * @return null|mixed
     */
    protected function extractItems(ReflectionClass $class)
    {
        return $this->withMetadata(static fn (ClassMetadata $metadata) => $metadata->fieldMappings, $class);
    }

    protected function decorateItem(ReflectionClass $class, NodeInterface $node, ClassVisitorInterface $visitor, array $field): void
    {
        $isIdentifier = $this->withMetadata(static fn (ClassMetadata $metadata) => $metadata->isIdentifier($field['fieldName']), $class);

        $node->addAttribute(new Field($field['fieldName'] ?? '', $field['type'] ?? null, $field['unique'] ?? false, $field['nullable'] ?? false, $isIdentifier));
    }
}
