<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Doctrine;

use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Reech\PlantUmlBundle\Model\Arrow\BaseArrow;
use Reech\PlantUmlBundle\Model\ClassVisitorInterface;
use Reech\PlantUmlBundle\Model\Decorator\InheritableItemDecoratorTrait;
use Reech\PlantUmlBundle\Model\Node\Member\Member;
use Reech\PlantUmlBundle\Model\NodeInterface;
use ReflectionClass;

/**
 * Description of RelationDecorator.
 */
class AssociationDecorator extends AbstractDoctrineDecorator
{
    use InheritableItemDecoratorTrait;

    public function toConfig(array &$conf): void
    {
        $conf['decorators'][] = 'associations';
    }

    /**
     * @param ReflectionClass $class
     *
     * @return null|mixed
     */
    protected function extractItems(ReflectionClass $class)
    {
        return $this->withMetadata(static fn (ClassMetadata $metadata) => $metadata->getAssociationMappings(), $class);
    }

    protected function decorateItem(ReflectionClass $class, NodeInterface $node, ClassVisitorInterface $visitor, array $association): void
    {
        if (!$association['isOwningSide']) {
            return;
        }

        $target = $visitor->visitClass($association['targetEntity']);
        if (false === $target) {
            $type = $association['targetEntity'];
            if ($association['type'] & ClassMetadataInfo::TO_MANY !== 0) {
                $type .= '[]';
            }
            $node->addAttribute(new Member($association['fieldName'], $type));

            return;
        }

        $linkSource = '';
        $linkTarget = '>';
        if ($association['isCascadeRemove']) {
            $linkSource = 'o';
        }

        $sourceCardinality = '';
        $targetCardinality = '';

        switch ($association['type']) {
            case ClassMetadataInfo::ONE_TO_ONE:
                $sourceCardinality = '1';
                $targetCardinality = '1';

                break;

            case ClassMetadataInfo::ONE_TO_MANY:
                $sourceCardinality = '1';
                $targetCardinality = '*';

                break;

            case ClassMetadataInfo::MANY_TO_MANY:
                $sourceCardinality = '*';
                $targetCardinality = '*';

                break;

            case ClassMetadataInfo::MANY_TO_ONE:
                $sourceCardinality = '*';
                $targetCardinality = '1';

                break;
        }

        $node->addArrow(new BaseArrow($node, $target, '--', $association['fieldName'].' >', $linkSource, $linkTarget, $sourceCardinality, $targetCardinality));
    }
}
