<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Doctrine;

use ArrayIterator;
use Doctrine\ORM\EntityManager;
use Reech\PlantUmlBundle\Finder\FinderInterface;

/**
 * Class EntityFinder.
 */
class EntityFinder implements FinderInterface
{
    private EntityManager $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getIterator(): ArrayIterator
    {
        $classes = [];
        foreach ($this->entityManager->getMetadataFactory()->getAllMetadata() as $metadata) {
            $classes[$metadata->getName()] = $metadata->getReflectionClass();
        }

        return new ArrayIterator($classes);
    }

    public function toConfig(array &$conf): void
    {
        $conf['type'] = 'entities';
    }
}
