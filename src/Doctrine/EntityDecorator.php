<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Doctrine;

use Reech\PlantUmlBundle\Model\ClassVisitorInterface;
use Reech\PlantUmlBundle\Model\NodeInterface;
use ReflectionClass;

/**
 * Description of EntityDecorator.
 */
class EntityDecorator extends AbstractDoctrineDecorator
{
    public function decorate(ReflectionClass $class, NodeInterface $node, ClassVisitorInterface $visitor): void
    {
        $this->withMetadata(static function ($metadata) use ($node): void {
            if ($metadata->isMappedSuperclass) {
                $node->addStereotype('mappedSuperClass');
            } elseif ($metadata->isEmbeddedClass) {
                $node->addStereotype('embedded');
            } else {
                $node->addStereotype('entity');
            }
        }, $class);
    }

    public function toConfig(array &$conf): void
    {
        $conf['decorators'][] = 'entity';
    }
}
