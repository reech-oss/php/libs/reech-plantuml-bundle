<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Doctrine;

use Reech\PlantUmlBundle\Model\Node\Member\Member;
use Reech\PlantUmlBundle\Writer\WritableInterface;
use Reech\PlantUmlBundle\Writer\WriterInterface;

/**
 * Description of Field.
 */
class Field extends Member
{
    private bool $isUnique;
    private bool $isNullable;
    private bool $isIdentifier;

    public function __construct(string $symbol, string $type, bool $isUnique = false, bool $isNullable = false, bool $isIdentifier = false)
    {
        parent::__construct($symbol, $type);
        $this->isUnique = $isUnique;
        $this->isNullable = $isNullable;
        $this->isIdentifier = $isIdentifier;
    }

    protected function writeSymbolTo(WriterInterface $writer): WritableInterface
    {
        if ($this->isIdentifier) {
            $writer->write('<b>');
        }
        if (!$this->isNullable) {
            $writer->write('<i>');
        }
        if ($this->isUnique) {
            $writer->write('<u>');
        }
        parent::writeSymbolTo($writer);
        if ($this->isUnique) {
            $writer->write('</u>');
        }
        if (!$this->isNullable) {
            $writer->write('</i>');
        }
        if ($this->isIdentifier) {
            $writer->write('</b>');
        }

        return $this;
    }
}
