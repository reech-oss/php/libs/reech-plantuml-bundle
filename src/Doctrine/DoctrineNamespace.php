<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Doctrine;

use Doctrine\ORM\EntityManagerInterface;
use Reech\PlantUmlBundle\Model\Namespace_\MappedNamespace;

/**
 * Description of DoctrineNamespace.
 */
class DoctrineNamespace extends MappedNamespace
{
    public const CONF_TYPE = 'entities';
    public const SEPARATOR = '::';

    public function __construct(EntityManagerInterface $em)
    {
        $entityNamespaces = $em->getConfiguration()->getEntityNamespaces();

        $mapping = [];
        foreach ($entityNamespaces as $alias => $namespace) {
            $mapping[$namespace.'\\'] = $alias.'::';
        }
        parent::__construct($mapping);
    }
}
