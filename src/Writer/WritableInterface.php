<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Writer;

/**
 * Interface WritableInterface.
 */
interface WritableInterface
{
    public function writeTo(WriterInterface $writer): ?self;
}
