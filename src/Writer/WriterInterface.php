<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Writer;

/**
 * Interface WriterInterface.
 */
interface WriterInterface
{
    public function indent(int $n = 1): self;

    public function dedent(int $n = 1): self;

    public function write(string $data): self;

    public function writeFormatted(string $fmt, ...$othersArgs): self;
}
