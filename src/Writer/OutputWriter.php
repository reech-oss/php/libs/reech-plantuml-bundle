<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Writer;

use Symfony\Component\Console\Output\OutputInterface;

/**
 * Description of OutputWriter.
 */
class OutputWriter extends AbstractWriter
{
    private OutputInterface $output;

    public function __construct(OutputInterface $output)
    {
        $this->output = $output;
    }

    protected function doWrite(string $data): void
    {
        $this->output->write($data, false, OutputInterface::OUTPUT_RAW);
    }
}
