<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Writer;

use function mb_strlen;

/**
 * Description of AbstractWriter.
 */
abstract class AbstractWriter implements WriterInterface
{
    private int $indentation = 0;
    private ?string $buffer = null;

    public function dedent(int $n = 1): WriterInterface
    {
        $this->indentation = max(0, $this->indentation - $n);

        return $this;
    }

    public function indent(int $n = 1): WriterInterface
    {
        $this->indentation += $n;

        return $this;
    }

    public function writeFormatted(string $format, ...$othersArgs): WriterInterface
    {
        $this->write(vsprintf($format, $othersArgs));

        return $this;
    }

    public function write(string $data): WriterInterface
    {
        $buffer = $this->buffer.$data;
        $indentation = str_repeat('  ', $this->indentation);
        $current = 0;
        while ($current < mb_strlen($buffer) && false !== ($next = mb_strpos($buffer, "\n", $current))) {
            ++$next;
            $this->doWrite($indentation.mb_substr($buffer, $current, $next - $current));
            $current = $next;
        }
        $this->buffer = mb_substr($buffer, $current);

        return $this;
    }

    /**
     * @param string $data
     */
    abstract protected function doWrite(string $data): void;
}
