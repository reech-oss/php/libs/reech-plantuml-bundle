<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Writer;

/**
 * Interface WritableArrowInterface.
 */
interface WritableArrowInterface extends WritableInterface
{
}
