<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Writer;

use InvalidArgumentException;

use function gettype;
use function is_resource;

/**
 * Description of OutputWriter.
 */
class StreamWriter extends AbstractWriter
{
    /**
     * @var resource
     */
    private $stream;

    /**
     * StreamWriter constructor.
     *
     * @param resource $stream
     */
    public function __construct($stream)
    {
        if (!is_resource($stream)) {
            throw new InvalidArgumentException(sprintf('Expecting a stream, not a %s', gettype($stream)));
        }
        $this->stream = $stream;
    }

    protected function doWrite(string $data): void
    {
        fwrite($this->stream, $data);
    }
}
