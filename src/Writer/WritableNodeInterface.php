<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Writer;

/**
 * Interface WritableNodeInterface.
 */
interface WritableNodeInterface extends WritableInterface
{
    public function writeAliasTo(WriterInterface $writer);
}
