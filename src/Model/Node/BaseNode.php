<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Node;

use Reech\PlantUmlBundle\Model\ArrowInterface;
use Reech\PlantUmlBundle\Model\NamespaceInterface;
use Reech\PlantUmlBundle\Model\Node\Member\AttributeInterface;
use Reech\PlantUmlBundle\Model\Node\Member\MethodInterface;
use Reech\PlantUmlBundle\Model\NodeInterface;
use Reech\PlantUmlBundle\Writer\WritableInterface;
use Reech\PlantUmlBundle\Writer\WriterInterface;

/**
 * Description of Class.
 */
class BaseNode implements NodeInterface
{
    private string $label;
    private string $id;
    private string $nodeType;
    /**
     * @var string[]
     */
    private array $classifiers;
    /**
     * @var string[]
     */
    private array $stereotypes;
    private NamespaceInterface $namespace;
    /**
     * @var AttributeInterface[]
     */
    private array $attributes = [];
    /**
     * @var MethodInterface[]
     */
    private array $methods = [];

    public function __construct(NamespaceInterface $namespace, string $name, string $nodeType, array $classifiers = [], array $stereotypes = [])
    {
        $this->namespace = $namespace;
        $this->id = $namespace->getNodeId($name);
        $this->label = $namespace->getNodeLabel($name);

        $this->nodeType = $nodeType;
        $this->classifiers = $classifiers;
        $this->stereotypes = $stereotypes;
    }

    public function addClassifier(string $classifier): NodeInterface
    {
        $this->classifiers[] = $classifier;

        return $this;
    }

    public function addStereotype(string $stereotype): NodeInterface
    {
        $this->stereotypes[] = $stereotype;

        return $this;
    }

    public function addAttribute(AttributeInterface $attribute): NodeInterface
    {
        $this->attributes[] = $attribute;

        return $this;
    }

    public function addMethod(MethodInterface $method): NodeInterface
    {
        $this->methods[] = $method;

        return $this;
    }

    public function setLabel(string $label): NodeInterface
    {
        $this->label = $label;

        return $this;
    }

    public function writeTo(WriterInterface $writer): ?WritableInterface
    {
        $this
            ->writeClassifiersTo($writer)
            ->writeNodeTypeTo($writer)
        ;
        $writer->writeFormatted('"%s" as %s', $this->label, $this->id);
        $this
            ->writeStereotypesTo($writer)
        ;
        $writer
            ->write(" {\n")
            ->indent()
        ;
        $this
            ->writeAttributesTo($writer)
            ->writeMethodsTo($writer)
        ;
        $writer
            ->dedent()
            ->write("}\n")
        ;

        return $this;
    }

    public function writeAliasTo(WriterInterface $writer): self
    {
        $writer->write($this->id);

        return $this;
    }

    public function addArrow(ArrowInterface $arrow): NodeInterface
    {
        $this->namespace->addArrow($arrow);

        return $this;
    }

    protected function writeClassifiersTo(WriterInterface $writer): self
    {
        foreach ($this->classifiers as $classifier) {
            $writer->writeFormatted('%s ', $classifier);
        }

        return $this;
    }

    protected function writeNodeTypeTo(WriterInterface $writer): self
    {
        $writer->writeFormatted('%s ', $this->nodeType);

        return $this;
    }

    protected function writeStereotypesTo(WriterInterface $writer): self
    {
        foreach ($this->stereotypes as $stereotype) {
            $writer->writeFormatted(' <<%s>>', $stereotype);
        }

        return $this;
    }

    protected function writeAttributesTo(WriterInterface $writer): self
    {
        foreach ($this->attributes as $attribute) {
            $attribute->writeTo($writer);
        }

        return $this;
    }

    protected function writeMethodsTo(WriterInterface $writer): self
    {
        foreach ($this->methods as $method) {
            $method->writeTo($writer);
        }

        return $this;
    }
}
