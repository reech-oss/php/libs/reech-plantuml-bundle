<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Node\Member;

/**
 * Interface AttributeInterface.
 */
interface AttributeInterface extends MemberInterface
{
}
