<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Node\Member;

/**
 * Interface MethodInterface.
 */
interface MethodInterface extends MemberInterface
{
}
