<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Node\Member;

use Reech\PlantUmlBundle\Writer\WritableInterface;

/**
 * Interface MemberInterface.
 */
interface MemberInterface extends WritableInterface
{
    public const PRIVATE_ = '-';
    public const PROTECTED_ = '#';
    public const PUBLIC_ = '+';
    public const UNKNOWN = '';
}
