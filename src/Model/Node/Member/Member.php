<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Node\Member;

use Reech\PlantUmlBundle\Model\TypedSymbol;
use Reech\PlantUmlBundle\Writer\WritableInterface;
use Reech\PlantUmlBundle\Writer\WriterInterface;

/**
 * Description of AbstractMember.
 */
class Member extends TypedSymbol implements MethodInterface, AttributeInterface
{
    private string $visibility;

    public function __construct(string $symbol, string $type = null, string $visibility = self::UNKNOWN)
    {
        parent::__construct($symbol, $type);
        $this->visibility = $visibility;
    }

    public function writeTo(WriterInterface $writer): ?WritableInterface
    {
        $writer->write($this->visibility);
        $this->writeMemberTo($writer);
        $writer->write("\n");

        return $this;
    }

    protected function writeMemberTo(WriterInterface $writer): ?WritableInterface
    {
        return parent::writeTo($writer);
    }
}
