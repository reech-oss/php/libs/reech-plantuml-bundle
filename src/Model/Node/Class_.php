<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Node;

use Reech\PlantUmlBundle\Model\NamespaceInterface;

/**
 * Description of Class_.
 *
 * @SuppressWarnings("PHPMD.CamelCaseClassName")
 */
class Class_ extends BaseNode
{
    public function __construct(NamespaceInterface $namespace, string $name, bool $isAbstract, bool $isFinal)
    {
        $classifiers = [];
        $stereotypes = [];
        if ($isAbstract) {
            $classifiers[] = 'abstract';
        } elseif ($isFinal) {
            $stereotypes[] = 'final';
        }
        parent::__construct($namespace, $name, 'class', $classifiers, $stereotypes);
    }
}
