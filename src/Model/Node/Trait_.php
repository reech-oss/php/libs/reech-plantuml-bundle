<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Node;

use Reech\PlantUmlBundle\Model\NamespaceInterface;

/**
 * Description of Trait_.
 *
 * @SuppressWarnings("PHPMD.CamelCaseClassName")
 */
class Trait_ extends BaseNode
{
    public function __construct(NamespaceInterface $namespace, $name)
    {
        parent::__construct($namespace, $name, 'class', [], ['trait']);
    }
}
