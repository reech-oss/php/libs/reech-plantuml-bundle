<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Node;

use Reech\PlantUmlBundle\Model\NamespaceInterface;

/**
 * Description of Interface_.
 *
 * @SuppressWarnings("PHPMD.CamelCaseClassName")
 */
class Interface_ extends BaseNode
{
    public function __construct(NamespaceInterface $namespace, $name)
    {
        parent::__construct($namespace, $name, 'interface', [], []);
    }
}
