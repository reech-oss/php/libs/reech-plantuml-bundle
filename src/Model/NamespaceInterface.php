<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model;

use Reech\PlantUmlBundle\Writer\WritableInterface;

/**
 * Interface NamespaceInterface.
 */
interface NamespaceInterface extends WritableInterface, ToConfigInterface
{
    public function getNamespace(string $namespaceName): self;

    public function getNodeId(string $className): string;

    public function getNodeLabel(string $className): string;

    public function addNode(NodeInterface $node): self;

    public function addArrow(ArrowInterface $arrow): self;
}
