<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model;

use Reech\PlantUmlBundle\Finder\FinderInterface;
use Reech\PlantUmlBundle\Writer\WritableInterface;
use Reech\PlantUmlBundle\Writer\WriterInterface;

/**
 * Description of Graph.
 */
class Graph implements GraphInterface
{
    private ClassVisitorInterface $visitor;
    private FinderInterface $finder;

    public function __construct(ClassVisitorInterface $visitor, FinderInterface $finder)
    {
        $this->visitor = $visitor;
        $this->finder = $finder;
    }

    public function visitAll(): void
    {
        foreach ($this->finder->getIterator() as $class) {
            $this->visitor->visitClass($class);
        }
    }

    public function writeTo(WriterInterface $writer): ?WritableInterface
    {
        $writer->write("@startuml@\n");
        $this->visitor->writeTo($writer);
        $writer->write("@enduml@\n");

        return $this;
    }

    public function toConfig(array &$conf): void
    {
        $conf['sources'] = [];
        $this->finder->toConfig($conf['sources']);
        $this->visitor->toConfig($conf);
    }
}
