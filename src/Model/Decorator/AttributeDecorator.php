<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Decorator;

use Reech\PlantUmlBundle\Model\ClassVisitorInterface;
use Reech\PlantUmlBundle\Model\DecoratorInterface;
use Reech\PlantUmlBundle\Model\Node\Member\Member;
use Reech\PlantUmlBundle\Model\Node\Member\MemberInterface;
use Reech\PlantUmlBundle\Model\NodeInterface;
use ReflectionClass;
use ReflectionProperty;

use function assert;

/**
 * Description of AttributeDecorator.
 */
class AttributeDecorator implements DecoratorInterface
{
    public function decorate(ReflectionClass $class, NodeInterface $node, ClassVisitorInterface $visitor): void
    {
        foreach ($class->getProperties() as $property) {
            assert($property instanceof ReflectionProperty);
            if ($property->getDeclaringClass()->getName() !== $class->getName()) {
                continue;
            }

            $memberVisibility = MemberInterface::UNKNOWN;

            switch (true) {
                case $property->isPrivate():
                    $memberVisibility = MemberInterface::PRIVATE_;

                    break;

                case $property->isProtected():
                    $memberVisibility = MemberInterface::PROTECTED_;

                    break;

                case $property->isPublic():
                    $memberVisibility = MemberInterface::PUBLIC_;

                    break;
            }
            $node->addAttribute(new Member($property->getName(), null, $memberVisibility));
        }
    }

    public function toConfig(array &$conf): void
    {
        $conf['decorators'][] = 'attributes';
    }
}
