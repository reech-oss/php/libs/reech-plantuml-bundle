<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Decorator;

use InvalidArgumentException;
use Reech\PlantUmlBundle\Model\Arrow\ImplementsInterface;
use Reech\PlantUmlBundle\Model\ArrowInterface;
use Reech\PlantUmlBundle\Model\ClassVisitorInterface;
use Reech\PlantUmlBundle\Model\Node\Interface_;
use Reech\PlantUmlBundle\Model\NodeInterface;
use ReflectionClass;

use function call_user_func_array;

/**
 * Description of InheritanceDecorator.
 */
class InterfaceDecorator extends AbstractRelationDecorator
{
    public function decorate(ReflectionClass $class, NodeInterface $node, ClassVisitorInterface $visitor): void
    {
        $interfaces = $class->getInterfaceNames();

        $indirectInterfaces = array_filter(array_map(static fn (ReflectionClass $i) => $i->getInterfaceNames(), $class->getInterfaces()));

        if (!empty($indirectInterfaces)) {
            /** @noinspection ArgumentUnpackingCanBeUsedInspection */
            $indirectInterfaces = call_user_func_array('array_merge', $indirectInterfaces);
            $interfaces = array_diff($interfaces, $indirectInterfaces);
        }

        $parent = $class->getParentClass();
        if ($parent) {
            $interfaces = array_diff($interfaces, $parent->getInterfaceNames());
        }

        $this->visitRelations($node, $interfaces, $visitor);
    }

    public function toConfig(array &$conf): void
    {
        $conf['decorators'][] = 'interfaces';
    }

    protected function buildRelation(NodeInterface $source, NodeInterface $target): ArrowInterface
    {
        if (!$target instanceof Interface_) {
            throw new InvalidArgumentException(sprintf('Invalid target parameter: not of type %s', Interface_::class));
        }

        return new ImplementsInterface($source, $target);
    }
}
