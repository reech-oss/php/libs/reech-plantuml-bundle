<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Decorator;

use Reech\PlantUmlBundle\Model\Arrow\ExtendsClass;
use Reech\PlantUmlBundle\Model\ArrowInterface;
use Reech\PlantUmlBundle\Model\ClassVisitorInterface;
use Reech\PlantUmlBundle\Model\NodeInterface;
use ReflectionClass;

/**
 * Description of InheritanceDecorator.
 */
class InheritanceDecorator extends AbstractRelationDecorator
{
    public function decorate(ReflectionClass $class, NodeInterface $node, ClassVisitorInterface $visitor): void
    {
        $parent = $class->getParentClass();
        if ($parent) {
            $this->visitRelations($node, [$parent->getName()], $visitor);
        }
    }

    public function toConfig(array &$conf): void
    {
        $conf['decorators'][] = 'inheritance';
    }

    protected function buildRelation(NodeInterface $source, NodeInterface $target): ArrowInterface
    {
        return new ExtendsClass($source, $target);
    }
}
