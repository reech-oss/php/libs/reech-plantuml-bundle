<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Decorator;

use Reech\PlantUmlBundle\Model\ClassVisitorInterface;
use Reech\PlantUmlBundle\Model\DecoratorInterface;
use Reech\PlantUmlBundle\Model\NodeInterface;
use ReflectionClass;

/**
 * Description of CompositeDecorator.
 */
class CompositeDecorator implements DecoratorInterface
{
    /**
     * @var DecoratorInterface[]
     */
    private array $decorators;

    public function __construct(array $decorators = [])
    {
        $this->decorators = $decorators;
    }

    public function addDecorator(DecoratorInterface $decorator): self
    {
        $this->decorators[] = $decorator;

        return $this;
    }

    public function decorate(ReflectionClass $class, NodeInterface $node, ClassVisitorInterface $visitor): void
    {
        foreach ($this->decorators as $decorator) {
            $decorator->decorate($class, $node, $visitor);
        }
    }

    public function toConfig(array &$conf): void
    {
        foreach ($this->decorators as $decorator) {
            $decorator->toConfig($conf);
        }
    }
}
