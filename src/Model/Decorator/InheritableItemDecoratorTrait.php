<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Decorator;

use Reech\PlantUmlBundle\Model\ClassVisitorInterface;
use Reech\PlantUmlBundle\Model\NodeInterface;
use ReflectionClass;

/**
 * Décorateur abstrait pour les éléments d'une classe qui peuvent être hérités d'une classe base.
 * Ce décorateur s'assure de n'afficher que les éléments propres à la classe.
 */
trait InheritableItemDecoratorTrait
{
    public function decorate(ReflectionClass $class, NodeInterface $node, ClassVisitorInterface $visitor): void
    {
        $items = $this->extractItems($class);
        if (empty($items)) {
            return;
        }

        $parent = $class->getParentClass();
        if ($parent) {
            $parentItems = $this->extractItems($parent);
            if (!empty($parentItems)) {
                $items = array_diff_key($items, $parentItems);
            }
        }

        foreach ($items as $item) {
            $this->decorateItem($class, $node, $visitor, $item);
        }
    }

    abstract protected function extractItems(ReflectionClass $class);

    abstract protected function decorateItem(ReflectionClass $class, NodeInterface $node, ClassVisitorInterface $visitor, $item);
}
