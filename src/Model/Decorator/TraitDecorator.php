<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Decorator;

use Reech\PlantUmlBundle\Model\Arrow\UsesTrait;
use Reech\PlantUmlBundle\Model\ArrowInterface;
use Reech\PlantUmlBundle\Model\ClassVisitorInterface;
use Reech\PlantUmlBundle\Model\NodeInterface;
use ReflectionClass;

/**
 * Description of InheritanceDecorator.
 */
class TraitDecorator extends AbstractRelationDecorator
{
    public function decorate(ReflectionClass $class, NodeInterface $node, ClassVisitorInterface $visitor): void
    {
        $traits = $class->getTraitNames();

        $parent = $class->getParentClass();
        if ($parent) {
            $traits = array_diff($traits, $parent->getTraitNames());
        }

        $this->visitRelations($node, $traits, $visitor);
    }

    public function toConfig(array &$conf): void
    {
        $conf['decorators'][] = 'traits';
    }

    protected function buildRelation(NodeInterface $source, NodeInterface $target): ArrowInterface
    {
        return new UsesTrait($source, $target);
    }
}
