<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Decorator;

use Reech\PlantUmlBundle\Model\ClassVisitorInterface;
use Reech\PlantUmlBundle\Model\DecoratorInterface;
use Reech\PlantUmlBundle\Model\NodeInterface;
use Reech\PlantUmlBundle\Utils\SingletonTrait;
use ReflectionClass;

/**
 * Description of NullDecorator.
 */
class NullDecorator implements DecoratorInterface
{
    use SingletonTrait;

    public function decorate(ReflectionClass $class, NodeInterface $node, ClassVisitorInterface $visitor): void
    {
    }

    public function toConfig(array &$conf): void
    {
    }
}
