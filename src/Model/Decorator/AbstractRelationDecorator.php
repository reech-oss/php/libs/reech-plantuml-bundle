<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Decorator;

use Reech\PlantUmlBundle\Model\ArrowInterface;
use Reech\PlantUmlBundle\Model\ClassVisitorInterface;
use Reech\PlantUmlBundle\Model\DecoratorInterface;
use Reech\PlantUmlBundle\Model\NodeInterface;

/**
 * Description of InheritanceDecorator.
 */
abstract class AbstractRelationDecorator implements DecoratorInterface
{
    protected function visitRelations(NodeInterface $source, array $classNames, ClassVisitorInterface $visitor): void
    {
        foreach ($classNames as $className) {
            $target = $visitor->visitClass($className);
            if ($target) {
                $source->addArrow($this->buildRelation($source, $target));
            }
        }
    }

    abstract protected function buildRelation(NodeInterface $source, NodeInterface $target): ArrowInterface;
}
