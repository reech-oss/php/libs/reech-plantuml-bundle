<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Decorator;

use Reech\PlantUmlBundle\Model\ClassFilterInterface;
use Reech\PlantUmlBundle\Model\ClassVisitorInterface;
use Reech\PlantUmlBundle\Model\DecoratorInterface;
use Reech\PlantUmlBundle\Model\NodeInterface;
use ReflectionClass;

/**
 * Description of FilteringDecorator.
 */
class FilteringDecorator implements DecoratorInterface
{
    private DecoratorInterface $next;
    private ClassFilterInterface $filter;

    public function __construct(DecoratorInterface $next, ClassFilterInterface $filter)
    {
        $this->next = $next;
        $this->filter = $filter;
    }

    public function decorate(ReflectionClass $class, NodeInterface $node, ClassVisitorInterface $visitor): void
    {
        if ($this->filter->accept($class)) {
            $this->next->decorate($class, $node, $visitor);
        }
    }

    public function toConfig(array &$conf): void
    {
        $this->filter->toConfig($conf);
        $this->next->toConfig($conf);
    }
}
