<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Decorator;

use Reech\PlantUmlBundle\Model\ClassVisitorInterface;
use Reech\PlantUmlBundle\Model\DecoratorInterface;
use Reech\PlantUmlBundle\Model\Node\Member\Member;
use Reech\PlantUmlBundle\Model\Node\Member\MemberInterface;
use Reech\PlantUmlBundle\Model\NodeInterface;
use ReflectionClass;
use ReflectionMethod;

use function assert;

/**
 * Description of AttributeDecorator.
 */
class MethodDecorator implements DecoratorInterface
{
    public function decorate(ReflectionClass $class, NodeInterface $node, ClassVisitorInterface $visitor): void
    {
        foreach ($class->getMethods() as $method) {
            assert($method instanceof ReflectionMethod);
            if ($this->isAccessor($method, $class) || $method->getDeclaringClass()->getName() !== $class->getName()) {
                continue;
            }

            $memberVisibility = MemberInterface::UNKNOWN;

            switch (true) {
                case $method->isPrivate():
                    $memberVisibility = MemberInterface::PRIVATE_;

                    break;

                case $method->isProtected():
                    $memberVisibility = MemberInterface::PROTECTED_;

                    break;

                case $method->isPublic():
                    $memberVisibility = MemberInterface::PUBLIC_;

                    break;
            }
            $node->addMethod(new Member($method->getName().'(...)', null, $memberVisibility));
        }
    }

    public function toConfig(array &$conf): void
    {
        $conf['decorators'][] = 'methods';
    }

    protected function isAccessor(ReflectionMethod $method, ReflectionClass $class): bool
    {
        if (!$method->isPublic() || $method->isAbstract() || $method->getDeclaringClass()->isInterface()) {
            return false;
        }
        $groups = [];
        if (0 === $method->getNumberOfParameters() && preg_match('/(?:get|is)(\w+)/', $method->getName(), $groups)) {
            $name = lcfirst($groups[1]);
        } elseif (1 === $method->getNumberOfParameters() && preg_match('/(?:set|add|remove)(\w+)/', $method->getName(), $groups)) {
            $name = lcfirst($groups[1]);
        } else {
            return false;
        }
        if (!$class->hasProperty($name)) {
            return false;
        }
        $property = $class->getProperty($name);

        return $property->isStatic() === $method->isStatic();
    }
}
