<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model;

use ReflectionClass;

/**
 * Interface DecoratorInterface.
 */
interface DecoratorInterface extends ToConfigInterface
{
    public function decorate(ReflectionClass $class, NodeInterface $node, ClassVisitorInterface $visitor): void;
}
