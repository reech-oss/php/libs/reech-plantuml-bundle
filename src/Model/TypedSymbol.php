<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model;

use Reech\PlantUmlBundle\Writer\WritableInterface;
use Reech\PlantUmlBundle\Writer\WriterInterface;

/**
 * Description of TypedSymbol.
 */
class TypedSymbol implements WritableInterface
{
    private string $symbol;
    private ?string $type = null;

    public function __construct(string $symbol, string $type = null)
    {
        $this->symbol = $symbol;
        $this->type = $type;
    }

    public function writeTo(WriterInterface $writer): ?WritableInterface
    {
        return $this->writeTypeTo($writer)->writeSymbolTo($writer);
    }

    protected function writeTypeTo(WriterInterface $writer): self
    {
        if ($this->type) {
            $writer->writeFormatted('%s ', $this->type);
        }

        return $this;
    }

    protected function writeSymbolTo(WriterInterface $writer): WritableInterface
    {
        $writer->write($this->symbol);

        return $this;
    }
}
