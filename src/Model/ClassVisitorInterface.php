<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model;

use Reech\PlantUmlBundle\Writer\WritableInterface;
use ReflectionClass;

/**
 * Interface ClassVisitorInterface.
 */
interface ClassVisitorInterface extends WritableInterface, ToConfigInterface
{
    /**
     * @param ReflectionClass|string $classOrName
     *
     * @return bool|NodeInterface
     */
    public function visitClass($classOrName);
}
