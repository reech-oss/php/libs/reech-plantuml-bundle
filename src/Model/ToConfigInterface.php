<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model;

/**
 * Interface ToConfigInterface.
 */
interface ToConfigInterface
{
    public function toConfig(array &$conf): void;
}
