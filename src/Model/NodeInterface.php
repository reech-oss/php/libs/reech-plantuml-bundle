<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model;

use Reech\PlantUmlBundle\Model\Node\Member\AttributeInterface;
use Reech\PlantUmlBundle\Model\Node\Member\MethodInterface;
use Reech\PlantUmlBundle\Writer\WritableNodeInterface;

/**
 * Interface NodeInterface.
 */
interface NodeInterface extends WritableNodeInterface
{
    public function setLabel(string $label): self;

    public function addArrow(ArrowInterface $arrow): self;

    public function addClassifier(string $classifier): self;

    public function addStereotype(string $stereotype): self;

    public function addAttribute(AttributeInterface $attribute): self;

    public function addMethod(MethodInterface $method): self;
}
