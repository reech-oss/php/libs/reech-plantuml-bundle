<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model;

use Reech\PlantUmlBundle\Writer\WritableArrowInterface;

/**
 * Interface ArrowInterface.
 */
interface ArrowInterface extends WritableArrowInterface
{
}
