<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Filter;

use Reech\PlantUmlBundle\Model\ClassFilterInterface;
use Reech\PlantUmlBundle\Utils\SingletonTrait;
use ReflectionClass;

/**
 * Description of AcceptAllFilter.
 */
class AcceptAllFilter implements ClassFilterInterface
{
    use SingletonTrait;

    public function accept(ReflectionClass $class): bool
    {
        return true;
    }

    public function toConfig(array &$conf): void
    {
    }
}
