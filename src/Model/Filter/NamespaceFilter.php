<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Filter;

use ReflectionClass;

/**
 * Description of DirectoryFilter.
 */
class NamespaceFilter extends AbstractListFilter
{
    public const CONF_TYPE = 'namespaces';

    protected function extract(ReflectionClass $class): string
    {
        return $class->getNamespaceName();
    }

    protected function matches(string $tested, string $reference): bool
    {
        return 0 === mb_strpos($tested, $reference);
    }

    protected function normalize(string $namespace): string
    {
        return trim($namespace, '\\').'\\';
    }
}
