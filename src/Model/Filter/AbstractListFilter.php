<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Filter;

use Reech\PlantUmlBundle\Model\ClassFilterInterface;
use ReflectionClass;

use function array_key_exists;

/**
 * Description of AbstractListFilter.
 */
abstract class AbstractListFilter implements ClassFilterInterface
{
    /**
     * @var string[]
     */
    private array $allowed;
    private bool $notFound;

    public function __construct(array $allowed, bool $notFound = false)
    {
        $this->allowed = array_map([$this, 'normalize'], $allowed);
        $this->notFound = $notFound;
    }

    public function accept(ReflectionClass $class): bool
    {
        $tested = $this->normalize($this->extract($class));
        foreach ($this->allowed as $reference) {
            if ($this->matches($tested, $reference)) {
                return !$this->notFound;
            }
        }

        return $this->notFound;
    }

    public function toConfig(array &$conf): void
    {
        $key = $this->notFound ? 'exclude' : 'include';
        if (!array_key_exists($key, $conf)) {
            $conf[$key] = [];
        }
        $conf[$key][static::CONF_TYPE] = $this->allowed;
    }

    abstract protected function normalize(string $value): string;

    abstract protected function extract(ReflectionClass $class): string;

    abstract protected function matches(string $tested, string $reference): bool;
}
