<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Filter;

use ReflectionClass;
use Webmozart\PathUtil\Path;

use const DIRECTORY_SEPARATOR;

/**
 * Description of DirectoryFilter.
 */
class DirectoryFilter extends AbstractListFilter
{
    public const CONF_TYPE = 'directories';

    protected function extract(ReflectionClass $class): string
    {
        $filename = $class->getFileName();
        if (false === $filename) {
            return '';
        }

        return Path::getDirectory($filename);
    }

    protected function matches(string $tested, string $reference): bool
    {
        return 0 === mb_strpos($tested, $reference);
    }

    protected function normalize(string $path): string
    {
        return Path::canonicalize($path).DIRECTORY_SEPARATOR;
    }
}
