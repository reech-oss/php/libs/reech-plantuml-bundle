<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Filter;

use ReflectionClass;

/**
 * Description of DirectoryFilter.
 */
class ClassFilter extends AbstractListFilter
{
    public const CONF_TYPE = 'classes';

    protected function extract(ReflectionClass $class): string
    {
        return $class->getName();
    }

    protected function matches(string $tested, string $reference): bool
    {
        return $tested === $reference;
    }

    protected function normalize(string $className): string
    {
        return trim($className, '\\');
    }
}
