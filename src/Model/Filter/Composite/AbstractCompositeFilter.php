<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Filter\Composite;

use Reech\PlantUmlBundle\Model\ClassFilterInterface;

/**
 * Description of CompositeFilter.
 */
abstract class AbstractCompositeFilter implements ClassFilterInterface
{
    /**
     * @var ClassFilterInterface[]
     */
    protected array $filters;

    public function __construct(array $filters)
    {
        $this->filters = $filters;
    }

    public function toConfig(array &$conf): void
    {
        foreach ($this->filters as $filter) {
            $filter->toConfig($conf);
        }
    }
}
