<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Filter\Composite;

use ReflectionClass;

/**
 * Description of AnyFilter.
 */
class AnyFilter extends AbstractCompositeFilter
{
    public function accept(ReflectionClass $class): bool
    {
        foreach ($this->filters as $filter) {
            if ($filter->accept($class)) {
                return true;
            }
        }

        return false;
    }
}
