<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model;

use Reech\PlantUmlBundle\Writer\WritableInterface;

/**
 * Interface GraphInterface.
 */
interface GraphInterface extends WritableInterface, ToConfigInterface
{
    public function visitAll(): void;
}
