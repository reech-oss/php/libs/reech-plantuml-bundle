<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model;

use ReflectionClass;

/**
 * Interface ClassFilterInterface.
 */
interface ClassFilterInterface extends ToConfigInterface
{
    public function accept(ReflectionClass $class): bool;
}
