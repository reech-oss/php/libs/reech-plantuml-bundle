<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Namespace_\Php;

use Reech\PlantUmlBundle\Model\Namespace_\AbstractNamespace as Base;
use Reech\PlantUmlBundle\Model\NamespaceInterface;
use Reech\PlantUmlBundle\Writer\WriterInterface;

/**
 * Description of Namespace.
 */
abstract class AbstractNamespace extends Base
{
    /**
     * @var NamespaceInterface[]
     */
    private array $children = [];

    /**
     * @SuppressWarnings(PHPMD.ErrorControlOperator)
     */
    public function getNamespace(string $namespaceName): NamespaceInterface
    {
        $namespaceName = trim($namespaceName, '\\');
        if (!$namespaceName) {
            return $this;
        }
        @[$head, $tail] = explode('\\', $namespaceName, 2);
        if (!isset($this->children[$head])) {
            $this->children[$head] = new Namespace_($this, $head);
        }
        if (empty($tail)) {
            return $this->children[$head];
        }

        return $this->children[$head]->getNamespace($tail);
    }

    public function getNodeId(string $name): string
    {
        return str_replace('\\', '.', $name).'_node';
    }

    abstract protected function getNamespacePrefix(): string;

    protected function writeChildrenTo(WriterInterface $writer): self
    {
        foreach ($this->children as $child) {
            $child->writeTo($writer);
        }

        return $this;
    }

    protected function isEmpty(): bool
    {
        return parent::isEmpty() && empty($this->children);
    }
}
