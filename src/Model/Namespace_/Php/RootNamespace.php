<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Namespace_\Php;

use Reech\PlantUmlBundle\Model\ArrowInterface;
use Reech\PlantUmlBundle\Model\NamespaceInterface;
use Reech\PlantUmlBundle\Writer\WritableInterface;
use Reech\PlantUmlBundle\Writer\WriterInterface;

/**
 * Description of RootNamespace.
 */
class RootNamespace extends AbstractNamespace
{
    public const CONF_TYPE = 'php';

    /**
     * @var ArrowInterface[]
     */
    private array $arrows = [];

    public function addArrow(ArrowInterface $arrow): NamespaceInterface
    {
        $this->arrows[] = $arrow;

        return $this;
    }

    public function writeTo(WriterInterface $writer): ?WritableInterface
    {
        $writer->write("set namespaceSeparator .\n");
        $this
            ->writeNodesTo($writer)
            ->writeChildrenTo($writer)
            ->writeArrowsTo($writer)
        ;

        return $this;
    }

    public function getNodeLabel(string $className): string
    {
        return $className;
    }

    protected function writeArrowsTo(WriterInterface $writer): self
    {
        foreach ($this->arrows as $arrow) {
            $arrow->writeTo($writer);
        }

        return $this;
    }

    protected function getNamespacePrefix(): string
    {
        return '';
    }
}
