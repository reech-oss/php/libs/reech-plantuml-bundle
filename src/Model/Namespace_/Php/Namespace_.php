<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Namespace_\Php;

use Reech\PlantUmlBundle\Model\ArrowInterface;
use Reech\PlantUmlBundle\Model\NamespaceInterface;
use Reech\PlantUmlBundle\Writer\WritableInterface;
use Reech\PlantUmlBundle\Writer\WriterInterface;

use function mb_strlen;

/**
 * Description of Namespace.
 *
 * @SuppressWarnings("PHPMD.CamelCaseClassName")
 */
class Namespace_ extends AbstractNamespace
{
    private AbstractNamespace $parent;
    private string $name;

    public function __construct(AbstractNamespace $parent, string $name)
    {
        $this->parent = $parent;
        $this->name = $name;
    }

    public function addArrow(ArrowInterface $arrow): NamespaceInterface
    {
        $this->parent->addArrow($arrow);

        return $this;
    }

    public function getNodeLabel(string $name): string
    {
        $prefix = $this->getNamespacePrefix();
        if (0 === mb_strpos($name, $prefix)) {
            return mb_substr($name, mb_strlen($prefix));
        }

        return $name;
    }

    public function writeTo(WriterInterface $writer): ?WritableInterface
    {
        if ($this->isEmpty()) {
            return null;
        }

        $writer
            ->writeFormatted("namespace %s {\n", $this->name)
            ->indent()
        ;
        $this
            ->writeNodesTo($writer)
            ->writeChildrenTo($writer)
        ;
        $writer
            ->dedent()
            ->write("}\n")
        ;

        return $this;
    }

    protected function getNamespacePrefix(): string
    {
        return $this->parent->getNamespacePrefix().$this->name.'\\';
    }
}
