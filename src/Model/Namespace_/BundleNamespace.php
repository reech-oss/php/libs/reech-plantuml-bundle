<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Namespace_;

/**
 * Description of BundleNamespace.
 */
class BundleNamespace extends MappedNamespace
{
    public const CONF_TYPE = 'bundles';
    public const SEPARATOR = '::';

    public function __construct(array $bundles)
    {
        $mapping = [];
        foreach ($bundles as $bundle => $php) {
            $mapping[mb_substr($php, 0, 1 + mb_strrpos($php, '\\'))] = $bundle.'::';
        }

        parent::__construct($mapping);
    }
}
