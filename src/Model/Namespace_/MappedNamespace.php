<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Namespace_;

/**
 * Description of BundleNamespace.
 */
class MappedNamespace extends FlatNamespace
{
    public const SEPARATOR = '::';

    /**
     * @var string[]
     */
    private array $mappedNamespaces;
    /**
     * @var string[]
     */
    private array $phpNamespaces;

    public function __construct(array $mapping)
    {
        $this->phpNamespaces = array_keys($mapping);
        $this->mappedNamespaces = array_values($mapping);
    }

    public function getNodeId(string $name): string
    {
        /** @noinspection CascadeStringReplacementInspection */

        return str_replace('\\', '__', str_replace($this->phpNamespaces, $this->mappedNamespaces, $name));
    }

    public function getNodeLabel(string $name): string
    {
        return str_replace($this->phpNamespaces, '', $name);
    }
}
