<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Namespace_;

use Reech\PlantUmlBundle\Model\NamespaceInterface;
use Reech\PlantUmlBundle\Model\NodeInterface;
use Reech\PlantUmlBundle\Writer\WritableInterface;
use Reech\PlantUmlBundle\Writer\WriterInterface;

/**
 * Description of Namespace.
 */
abstract class AbstractNamespace implements WritableInterface, NamespaceInterface
{
    /**
     * @var NodeInterface[]
     */
    private array $nodes = [];

    public function addNode(NodeInterface $node): NamespaceInterface
    {
        $this->nodes[] = $node;

        return $this;
    }

    public function toConfig(array &$conf): void
    {
        $conf['namespace'] = static::CONF_TYPE;
    }

    protected function writeNodesTo(WriterInterface $writer): self
    {
        foreach ($this->nodes as $node) {
            $node->writeTo($writer);
        }

        return $this;
    }

    protected function isEmpty(): bool
    {
        return empty($this->nodes);
    }
}
