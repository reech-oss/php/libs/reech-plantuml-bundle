<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Namespace_;

use Reech\PlantUmlBundle\Model\ArrowInterface;
use Reech\PlantUmlBundle\Model\NamespaceInterface;
use Reech\PlantUmlBundle\Writer\WritableInterface;
use Reech\PlantUmlBundle\Writer\WriterInterface;

/**
 * Description of RootNamespace.
 */
class FlatNamespace extends AbstractNamespace
{
    public const CONF_TYPE = 'flat';
    public const SEPARATOR = 'none';

    /**
     * @var ArrowInterface[]
     */
    private array $arrows = [];

    public function addArrow(ArrowInterface $arrow): NamespaceInterface
    {
        $this->arrows[] = $arrow;

        return $this;
    }

    public function writeTo(WriterInterface $writer): ?WritableInterface
    {
        $writer->writeFormatted("set namespaceSeparator %s\n", static::SEPARATOR);
        $this
            ->writeNodesTo($writer)
            ->writeArrowsTo($writer)
        ;

        return $this;
    }

    public function getNamespace(string $namespaceName): NamespaceInterface
    {
        return $this;
    }

    public function getNodeId(string $name): string
    {
        return str_replace('\\', '_', $name);
    }

    public function getNodeLabel(string $name): string
    {
        return $name;
    }

    protected function writeArrowsTo(WriterInterface $writer): self
    {
        foreach ($this->arrows as $arrow) {
            $arrow->writeTo($writer);
        }

        return $this;
    }
}
