<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Arrow;

use InvalidArgumentException;
use Reech\PlantUmlBundle\Model\Node\Trait_;
use Reech\PlantUmlBundle\Model\NodeInterface;
use Reech\PlantUmlBundle\Writer\WritableNodeInterface;

/**
 * Description of UseTrait.
 */
class UsesTrait extends BaseArrow
{
    public function __construct(NodeInterface $source, NodeInterface $trait)
    {
        if (!$source instanceof WritableNodeInterface) {
            throw new InvalidArgumentException('Invalid type for source');
        }
        if (!$trait instanceof Trait_) {
            throw new InvalidArgumentException('Invalid type for trait');
        }

        parent::__construct($trait, $source, '--', null, '<|');
    }
}
