<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Arrow;

use InvalidArgumentException;
use Reech\PlantUmlBundle\Model\Node\Interface_;
use Reech\PlantUmlBundle\Model\NodeInterface;

/**
 * Description of ImplementsInterface.
 */
class ImplementsInterface extends BaseArrow
{
    public function __construct(NodeInterface $source, NodeInterface $target)
    {
        if (!$target instanceof Interface_) {
            throw new InvalidArgumentException('Invalid type for target');
        }

        parent::__construct($target, $source, '..', null, '<|');
    }
}
