<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Arrow;

use Reech\PlantUmlBundle\Model\ArrowInterface;
use Reech\PlantUmlBundle\Model\NodeInterface;
use Reech\PlantUmlBundle\Writer\WritableInterface;
use Reech\PlantUmlBundle\Writer\WriterInterface;

/**
 * Description of Arrow.
 */
class BaseArrow implements ArrowInterface
{
    private NodeInterface $source;
    private NodeInterface $target;
    private string $link;
    private ?string $label;
    private string $linkSource;
    private string $linkTarget;
    private string $sourceCardinality;
    private string $targetCardinality;

    public function __construct(NodeInterface $source, NodeInterface $target, string $link = '--', string $label = null, string $linkSource = '', string $linkTarget = '', string $sourceCardinality = '', string $targetCardinality = '')
    {
        $this->source = $source;
        $this->target = $target;
        $this->link = $link;
        $this->label = $label;
        $this->linkSource = $linkSource;
        $this->linkTarget = $linkTarget;
        $this->sourceCardinality = $sourceCardinality;
        $this->targetCardinality = $targetCardinality;
    }

    public function writeTo(WriterInterface $writer): ?WritableInterface
    {
        $this->source->writeAliasTo($writer);
        $this->writeLinkTo($writer);
        $this->target->writeAliasTo($writer);
        $this->writeLabelTo($writer);
        $writer->write("\n");

        return $this;
    }

    protected function writeLabelTo(WriterInterface $writer): self
    {
        if ($this->label) {
            $writer->writeFormatted(' : %s', $this->label);
        }

        return $this;
    }

    protected function writeLinkTo(WriterInterface $writer, bool $linkSource = false, bool $linkTarget = false): self
    {
        $this->writeSourceCardinalityTo($writer);
        $writer->writeFormatted(' %s%s%s ', $linkSource ?: $this->linkSource, $this->link, $linkTarget ?: $this->linkTarget);
        $this->writeTargetCardinalityTo($writer);

        return $this;
    }

    protected function writeSourceCardinalityTo(WriterInterface $writer): self
    {
        if ('' !== $this->sourceCardinality) {
            $writer->writeFormatted(' "%s"', $this->sourceCardinality);
        }

        return $this;
    }

    protected function writeTargetCardinalityTo(WriterInterface $writer): self
    {
        if ('' !== $this->targetCardinality) {
            $writer->writeFormatted('"%s" ', $this->targetCardinality);
        }

        return $this;
    }
}
