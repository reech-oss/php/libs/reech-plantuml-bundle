<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model\Arrow;

use InvalidArgumentException;
use Reech\PlantUmlBundle\Model\NodeInterface;
use Reech\PlantUmlBundle\Writer\WritableNodeInterface;

/**
 * Description of ExtendsClass.
 */
class ExtendsClass extends BaseArrow
{
    public function __construct(NodeInterface $source, NodeInterface $target)
    {
        if (!$source instanceof WritableNodeInterface) {
            throw new InvalidArgumentException('Invalid type for source');
        }
        if (!$target instanceof WritableNodeInterface) {
            throw new InvalidArgumentException('Invalid type for target');
        }

        parent::__construct($target, $source, '--', null, '<|');
    }
}
