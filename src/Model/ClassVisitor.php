<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\Model;

use InvalidArgumentException;
use Reech\PlantUmlBundle\Model\Decorator\NullDecorator;
use Reech\PlantUmlBundle\Model\Filter\AcceptAllFilter;
use Reech\PlantUmlBundle\Model\Namespace_\Php\RootNamespace;
use Reech\PlantUmlBundle\Model\Node\Class_;
use Reech\PlantUmlBundle\Model\Node\Interface_;
use Reech\PlantUmlBundle\Model\Node\Trait_;
use Reech\PlantUmlBundle\Writer\WritableInterface;
use Reech\PlantUmlBundle\Writer\WriterInterface;
use ReflectionClass;

use function is_string;

/**
 * Description of Visitor.
 *
 * @SuppressWarnings(PMD.CouplingBetweenObjects)
 */
class ClassVisitor implements ClassVisitorInterface
{
    private NamespaceInterface $rootNamespace;
    private ClassFilterInterface $filter;
    private DecoratorInterface $decorator;
    /**
     * @var NodeInterface[]
     */
    private array $nodes = [];

    public function __construct(DecoratorInterface $decorator = null, ClassFilterInterface $filter = null, NamespaceInterface $namespace = null)
    {
        $this->filter = $filter ?: AcceptAllFilter::instance();
        $this->decorator = $decorator ?: NullDecorator::instance();
        $this->rootNamespace = $namespace ?: new RootNamespace();
    }

    public function setClassFilter(ClassFilterInterface $filter): self
    {
        $this->filter = $filter;

        return $this;
    }

    public function setDecorator(DecoratorInterface $decorator): self
    {
        $this->decorator = $decorator;

        return $this;
    }

    /**
     * @param ReflectionClass|string $classOrName
     *
     * @return bool|NodeInterface
     */
    public function visitClass($classOrName)
    {
        if ($classOrName instanceof ReflectionClass) {
            $class = $classOrName;
        } elseif (is_string($classOrName)) {
            $class = new ReflectionClass($classOrName);
        } else {
            throw new InvalidArgumentException('Invalid argument, expected ReflectionClass or string');
        }
        $className = $class->getName();

        if (isset($this->nodes[$className])) {
            return $this->nodes[$className];
        }

        if (!$this->filter->accept($class)) {
            return $this->nodes[$className] = false;
        }

        $namespace = $this->rootNamespace->getNamespace($class->getNamespaceName());
        $node = $this->nodes[$className] = $this->createNode($namespace, $class);
        $namespace->addNode($node);

        $this->decorator->decorate($class, $node, $this);

        return $node;
    }

    public function writeTo(WriterInterface $writer): ?WritableInterface
    {
        return $this->rootNamespace->writeTo($writer);
    }

    public function toConfig(array &$conf): void
    {
        $conf['layout'] = [];
        $conf['decoration'] = [];

        $this->filter->toConfig($conf['layout']);
        $this->rootNamespace->toConfig($conf['layout']);
        $this->decorator->toConfig($conf['decoration']);
    }

    protected function createNode(NamespaceInterface $namespace, ReflectionClass $class): NodeInterface
    {
        $className = $class->getName();

        if ($class->isTrait()) {
            return new Trait_($namespace, $className);
        }

        if ($class->isInterface()) {
            return new Interface_($namespace, $className);
        }

        return new Class_($namespace, $className, $class->isAbstract(), $class->isFinal());
    }
}
