<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\DependencyInjection;

use Reech\PlantUmlBundle\DependencyInjection\Builder\ClassFilterBuilder;
use Reech\PlantUmlBundle\DependencyInjection\Builder\GraphDefinitionBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * Description of ReechPlantUmlExtension.
 */
class ReechPlantUmlExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('reech_plant_uml.binaries.java', $config['binaries']['java']);
        $container->setParameter('reech_plant_uml.binaries.plamtuml_jar', $config['binaries']['plamtuml_jar']);
        $container->setParameter('reech_plant_uml.binaries.dot', $config['binaries']['dot']);
        $container->setParameter('reech_plant_uml.output.directory', $config['output']['directory']);
        $container->setParameter('reech_plant_uml.output.format', $config['output']['format']);
        $container->setParameter('reech_plant_uml.graph_keys', array_keys($config['graphs']));

        $filterBuilder = new ClassFilterBuilder($container);
        foreach ($config['graphs'] as $key => $graph) {
            $id = "reech_plant_uml.graph.{$key}";
            $builder = new GraphDefinitionBuilder($container, $id, $graph, $filterBuilder);
            $container->setDefinition($id, $builder->build());
        }
    }
}
