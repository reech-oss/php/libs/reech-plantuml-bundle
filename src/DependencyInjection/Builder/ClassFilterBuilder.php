<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\DependencyInjection\Builder;

use Reech\PlantUmlBundle\Model\Filter\ClassFilter;
use Reech\PlantUmlBundle\Model\Filter\Composite\AllFilter;
use Reech\PlantUmlBundle\Model\Filter\DirectoryFilter;
use Reech\PlantUmlBundle\Model\Filter\NamespaceFilter;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

use function array_key_exists;
use function count;
use function is_array;
use function is_int;

/**
 * Description of ClassFilterBuilder.
 */
class ClassFilterBuilder
{
    private ContainerBuilder $container;
    /**
     * @var Reference[]
     */
    private array $filters = [];
    /**
     * @var string[]
     */
    private static array $filterClasses = [
        'classes' => ClassFilter::class,
        'directories' => DirectoryFilter::class,
        'namespaces' => NamespaceFilter::class,
    ];

    public function __construct(ContainerBuilder $container)
    {
        $this->container = $container;
    }

    public function build(array $config): ?Reference
    {
        $filters = $this->normalize(array_intersect_key($config, ['include' => true, 'exclude' => true]));
        if (null === $filters) {
            return null;
        }

        $hash = sha1(serialize($filters));
        if (!array_key_exists($hash, $this->filters)) {
            return $this->filters[$hash] = $this->doBuild("reech.plant_uml.filters.{$hash}", $filters);
        }

        return $this->filters[$hash];
    }

    /**
     * @param mixed $data
     *
     * @return mixed
     */
    protected function normalize($data)
    {
        if (null === $data || [] === $data) {
            return null;
        }
        if (is_array($data)) {
            $res = [];
            $isList = true;
            foreach ($data as $k => $v) {
                $normalized = $this->normalize($v);
                if (!empty($normalized)) {
                    $res[$k] = $normalized;
                    if (!is_int($k)) {
                        $isList = false;
                    }
                }
            }
            if (empty($res)) {
                return null;
            }
            if ($isList) {
                sort($res);
            } else {
                ksort($res);
            }

            return $res;
        }

        return $data;
    }

    protected function doBuild(string $id, array $config): ?Reference
    {
        $filters = [];

        if (isset($config['include'])) {
            $this->buildSubFilter($filters, "{$id}.include", $config['include'], false);
        }
        if (isset($config['exclude'])) {
            $this->buildSubFilter($filters, "{$id}.exclude", $config['exclude'], true);
        }

        if (empty($filters)) {
            return null;
        }

        if (1 === count($filters)) {
            return $filters[0];
        }

        $this->container->setDefinition($id, new Definition(AllFilter::class, [$filters]));

        return new Reference($id);
    }

    /**
     * @param array  $filters
     * @param string $id
     * @param array  $config
     * @param bool   $notFound
     */
    protected function buildSubFilter(array &$filters, string $id, array $config, bool $notFound): void
    {
        foreach (self::$filterClasses as $key => $class) {
            if (!isset($config[$key])) {
                continue;
            }
            $subId = "{$id}.{$key}";
            $def = new Definition($class, [$config[$key], $notFound]);
            $this->container->setDefinition($subId, $def);
            $filters[] = new Reference($subId);
        }
    }
}
