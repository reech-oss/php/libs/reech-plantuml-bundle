<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\DependencyInjection\Builder;

use InvalidArgumentException;
use Reech\PlantUmlBundle\Doctrine\EntityFinder;
use Reech\PlantUmlBundle\Finder\ClassFinder;
use Reech\PlantUmlBundle\Finder\FilteringFinder;
use Reech\PlantUmlBundle\Model\ClassVisitor;
use Reech\PlantUmlBundle\Model\Graph;
use Symfony\Component\DependencyInjection\ChildDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

use function count;
use function in_array;

/**
 * Description of GraphDefinitionBuilder.
 */
class GraphDefinitionBuilder
{
    private ContainerBuilder $container;
    private string $baseId;
    private array $config;
    private ClassFilterBuilder $filterBuilder;
    private Reference $entityManager;
    private ?Definition $definition = null;

    public function __construct(ContainerBuilder $container, string $baseId, array $config, ClassFilterBuilder $filterBuilder)
    {
        $this->container = $container;
        $this->baseId = $baseId;
        $this->config = $config;
        $this->filterBuilder = $filterBuilder;

        $emId = $config['sources']['entity_manager'];
        $this->entityManager = new Reference("doctrine.orm.{$emId}_entity_manager");
    }

    public function build(): Definition
    {
        if (!$this->definition) {
            return $this->definition = $this->doBuild();
        }

        return $this->definition;
    }

    protected function doBuild(): Definition
    {
        [$source, $sourceFilter] = $this->buildSources();

        $layoutFilter = $this->filterBuilder->build($this->config['layout']) ?: $sourceFilter;
        $decorator = $this->buildFilteredDecorator();
        $namespace = $this->buildNamespace();

        $visitor = $this->setDefinition('visitor', ClassVisitor::class, $decorator, $layoutFilter, $namespace);

        $def = new Definition(Graph::class, [$visitor, $source]);
        $def->setLazy(true);
        $def->setPublic(true);

        return $def;
    }

    /**
     * @return Reference[]
     */
    protected function buildSources(): array
    {
        $finder = $this->buildFinder();
        $filter = $this->filterBuilder->build($this->config['sources']);

        if ($filter) {
            $filtered = $this->setDefinition('finder', FilteringFinder::class, $finder, $filter);

            return [$filtered, $filter];
        }

        return [$finder, null];
    }

    protected function buildFinder(): Reference
    {
        $config = $this->config['sources'];

        switch ($config['type']) {
            case 'entities':
                return $this->setDefinition('finder.entities', EntityFinder::class, $this->entityManager);

            case 'classes':
                return $this->setDefinition('finder.classes', ClassFinder::class, $config['directories']);
        }

        throw new InvalidArgumentException("Invalid source type: {$config['type']}");
    }

    protected function buildFilteredDecorator(): Reference
    {
        $decorator = $this->buildDecorator();
        if (!$decorator) {
            return $decorator;
        }

        $filter = $this->filterBuilder->build($this->config['decoration']);
        if (!$filter) {
            return $decorator;
        }

        return $this->setDefinitionDecorator('decorator', 'reech.plant_uml.decorator.filtered.template', $decorator, $filter);
    }

    protected function buildDecorator(): ?Reference
    {
        $config = $this->config['decoration']['decorators'];

        if (empty($config)) {
            return null;
        }

        if (1 === count($config)) {
            return $this->buildTypedDecorator($config[0]);
        }

        $decorators = [];
        foreach ($config as $type) {
            $decorators[] = $this->buildTypedDecorator($type);
        }

        return $this->setDefinitionDecorator('decorator.all', 'reech.plant_uml.decorator.composite.template', $decorators);
    }

    protected function buildTypedDecorator(string $type): Reference
    {
        if (in_array($type, ['entity', 'associations', 'fields'], true)) {
            return $this->setDefinitionDecorator("decorator.{$type}", "reech.plant_uml.decorator.{$type}.template", $this->entityManager);
        }

        return new Reference("reech.plant_uml.decorator.{$type}");
    }

    protected function buildNamespace(): Reference
    {
        $type = $this->config['layout']['namespaces'];

        if ('entities' === $type) {
            return $this->setDefinitionDecorator("namespace.{$type}", "reech.plant_uml.namespaces.{$type}.template", $this->entityManager);
        }

        return $this->setDefinitionDecorator("namespace.{$type}", "reech.plant_uml.namespaces.{$type}.template");
    }

    /**
     * @param string            $localId
     * @param Definition|string $classOrDef
     * @param mixed             ...$arguments
     *
     * @return Reference
     */
    protected function setDefinition(string $localId, $classOrDef, ...$arguments): Reference
    {
        if ($classOrDef instanceof Definition) {
            $definition = $classOrDef;
        } else {
            $definition = new Definition($classOrDef, $arguments);
        }
        $id = $this->globalId($localId);
        $this->container->setDefinition($id, $definition);

        return new Reference($id);
    }

    protected function setDefinitionDecorator(string $localId, string $templateId, ...$arguments): Reference
    {
        $def = new ChildDefinition($templateId);
        $def->setArguments($arguments);

        return $this->setDefinition($localId, $def);
    }

    protected function globalId(string $localId): string
    {
        return "{$this->baseId}.{$localId}";
    }
}
