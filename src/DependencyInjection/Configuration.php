<?php

declare(strict_types=1);

namespace Reech\PlantUmlBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

use function dirname;

/**
 * Description of Configuration.
 */
class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('reech_plant_uml');

        $treeBuilder->getRootNode()
            ->children()
            ->arrayNode('output')
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('directory')
            ->info('Répertoire dans lequel écrire les fichiers.')
            ->defaultValue('%kernel.root_dir%/Resources/doc')
            ->end()
            ->enumNode('format')
            ->info('Format de sortie (cf. PlantUML).')
            ->defaultValue('svg')
            ->values(['png', 'svg', 'eps', 'pdf', 'vdx', 'html', 'xmi', 'txt', 'utxt'])
            ->end()
            ->end()
            ->end()
            ->arrayNode('binaries')
            ->info("Chemins vers les fichiers binaires, s'ils en sont pas dans PATH.")
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('java')
            ->info("Commande 'java'")
            ->defaultValue('java')
            ->end()
            ->scalarNode('plamtuml_jar')
            ->info('Archive du logiciel PlantUML')
            ->defaultValue(dirname(__DIR__, 2).'/bin/plantuml.jar')
            ->end()
            ->scalarNode('dot')
            ->info("Commande 'dot' du package 'graphviz'")
            ->defaultValue('dot')
            ->end()
            ->end()
            ->end()
            ->append($this->buildGraphNode())
            ->end()
        ;

        return $treeBuilder;
    }

    protected function buildGraphNode(): NodeDefinition
    {
        $node = (new TreeBuilder('graphs'))->getRootNode();

        $decorators = ['inheritance', 'traits', 'interfaces', 'entity', 'associations', 'methods', 'fields', 'attributes'];

        $node
            ->info('Graphes.')
            ->useAttributeAsKey('name')
            ->beforeNormalization()
            ->always(static function ($graphs) {
                foreach (array_keys($graphs) as $key) {
                    if (0 === mb_strpos($key, '.')) {
                        unset($graphs[$key]);
                    }
                }

                return $graphs;
            })
            ->end()
            ->prototype('array')
            ->info("description d'un graphe à générer.")
            ->children()
            ->arrayNode('sources')
            ->info('Sélection des classes à examiner.')
            ->addDefaultsIfNotSet()
            ->children()
            ->enumNode('type')
            ->info('Source de la liste de classes.')
            ->defaultValue('classes')
            ->values(['classes', 'entities'])
            ->end()
            ->scalarNode('entity_manager')
            ->info('Entity Manager à utiliser pour les entités.')
            ->defaultValue('default')
            ->end()
            ->arrayNode('directories')
            ->info('Répertoires contenant les sources.')
            ->defaultValue(['%kernel.root_dir%/../src'])
            ->prototype('scalar')->end()
            ->end()
            ->append($this->buildFilterNode('include', 'à inclure'))
            ->append($this->buildFilterNode('exclude', 'à ignorer'))
            ->end()
            ->end()
            ->arrayNode('layout')
            ->info('Configuration de la disposition et du parcours.')
            ->addDefaultsIfNotSet()
            ->children()
            ->enumNode('namespaces')
            ->info('Types .')
            ->defaultValue('php')
            ->values(['bundles', 'php', 'flat', 'entities'])
            ->end()
            ->append($this->buildFilterNode('include', 'à tracer'))
            ->append($this->buildFilterNode('exclude', 'à ne pas tracer'))
            ->end()
            ->end()
            ->arrayNode('decoration')
            ->info('Informations à afficher sur les classes.')
            ->addDefaultsIfNotSet()
            ->children()
            ->arrayNode('decorators')
            ->info('Liste des décorateurs à utiliser.')
            ->defaultValue($decorators)
            ->prototype('enum')
            ->values($decorators)
            ->end()
            ->end()
            ->append($this->buildFilterNode('include', 'à décorer'))
            ->append($this->buildFilterNode('exclude', 'à ne pas décorer'))
            ->end()
            ->end()
            ->end()
            ->end()
        ;

        return $node;
    }

    protected function buildFilterNode(string $nodeName, string $description): NodeDefinition
    {
        $node = (new TreeBuilder($nodeName))->getRootNode();

        $node
            ->children()
            ->arrayNode('directories')
            ->info("Répertoires {$description}")
            ->prototype('scalar')->end()
            ->end()
            ->arrayNode('namespaces')
            ->info("Namespaces {$description}")
            ->prototype('scalar')->end()
            ->end()
            ->arrayNode('classes')
            ->info("Classes {$description}")
            ->prototype('scalar')->end()
            ->end()
            ->end()
        ;

        return $node;
    }
}
